Name:    rpg_svo
Version: 0.10
Release: 1
Summary:        This is ROS noetic Package which implements a Semi-Direct Monocular Visual Odometry(SVO) 
Summary(zh_CN): 苏黎世大学的半直接法视觉里程计V1.0
License:	    Public Domain and Apache-2.0 and BSD and MIT and BSL-1.0 and LGPL-2.1-only and MPL-2.0 and GPL-3.0-only and GPL-2.0-or-later and MPL-1.1 and IJG and Zlib and OFL-1.1
URL:            https://gitee.com/src-openeuler/rpg_svo
Source0:        https://gitee.com/src-openeuler/rpg_svo/rpg_svo-0.10.tar.gz
BuildRequires:	gcc-c++
BuildRequires:	cmake
BuildRequires:	lz4-devel
BuildRequires:	bzip2-devel
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	openssl-devel
BuildRequires:	boost-devel
BuildRequires:	uuid-devel
BuildRequires:	uuid
BuildRequires:	libuuid-devel
BuildRequires:	libtiff-devel
BuildRequires:	libwebp-devel
BuildRequires:	libjpeg-turbo-devel
BuildRequires:	yaml-cpp-devel
BuildRequires:	python3-gpgme
BuildRequires:	gpgme-devel
BuildRequires:	cpp-gpgme
BuildRequires:	python3-crypto
BuildRequires:	boost-devel
BuildRequires:	libtool-ltdl-devel
BuildRequires:  libpng-devel
BuildRequires:	fltk-devel
BuildRequires:	bullet-devel
BuildRequires:	qt5
BuildRequires:	qt5-devel
BuildRequires:	python3-sip-devel
BuildRequires:	python3-qt5-devel
BuildRequires:	python3-pyqt5-sip
BuildRequires:	libXext-devel
BuildRequires:	python3-qt5
BuildRequires:	python3-sip
BuildRequires:  gtest-devel
BuildRequires:  gmock-devel

%description
This is ROS noetic Package which implements a Semi-Direct Monocular Visual Odometry(SVO)

%prep
%setup

%install
cd ./3rdparty/

cd empy-3.3.4/
python3 setup.py install --user
cd ..

cd six-1.15.0/
python3 setup.py install --user
cd ..

cd setuptools_scm-4.1.2/
python3 setup.py install --user
cd ..

cd python-dateutil-2.8.1/
python3 setup.py install --user
cd ..

cd pyparsing-2.4.7/
python3 setup.py install --user
cd ..

cd docutils-0.16/
python3 setup.py install --user
cd ..

cd catkin_pkg-0.4.23/
python3 setup.py install --user
cd ..

cd PyYAML-5.3.1/
python3 setup.py install --user
cd ..

cd distro-1.5.0/
python3 setup.py install --user
cd ..

cd rospkg-1.2.8/
python3 setup.py install --user
cd ..

cd tinyxml/

mkdir -p ../../install_isolated/include/tinyxml/
mkdir -p ../../install_isolated/lib/tinyxml/
make
cp tinystr.h ../../install_isolated/include/tinyxml/
cp tinyxml.h ../../install_isolated/include/tinyxml/
cp libtinyxml.so ../../install_isolated/lib/tinyxml/
cd ..

cd ..

#compile
./src/catkin/bin/catkin_make_isolated --install 

####
# 对install_isoloate内部的变量名称进行替换
#
####
SRC_PATH=$PWD/install_isolated
DST_PATH=/opt/ros/noetic
sed -i "s:${SRC_PATH}:${DST_PATH}:g"  `grep -rIln "${SRC_PATH}" install_isolated/*`

####
# 添加.catkin和.rosinstall文件
#
####
mkdir -p %{buildroot}/opt/ros/noetic/
cp -r install_isolated/* %{buildroot}/opt/ros/noetic/
cp  install_isolated/.rosinstall %{buildroot}/opt/ros/noetic/
cp  install_isolated/.catkin %{buildroot}/opt/ros/noetic/

%files
%defattr(-,root,root)
/opt/ros/noetic/*
/opt/ros/noetic/.rosinstall
/opt/ros/noetic/.catkin


%changelog
* Sat Sep 24 2022 Ryan chen <13794421285@163.com> 0.10-1
- build 0.10-1
